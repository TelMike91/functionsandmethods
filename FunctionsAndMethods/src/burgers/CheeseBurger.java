package burgers;

public class CheeseBurger implements Burger {
	private Burger hamburger;
	public CheeseBurger(Burger hamburger) {
		this.hamburger = hamburger;		
	}
	
	@Override
	public String toString() {
		return "Cheesy " + this.hamburger.toString();
	}
}
