package burgers;

public class KetchupBurger implements Burger  {
	private Burger burger;

	public KetchupBurger(Burger burger) {
		this.burger = burger;
	}
	
	@Override
	public String toString() {	
		return "Ketchupy " + burger.toString();
	}

}
