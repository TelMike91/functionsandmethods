package restaurant;
import burgers.Burger;
import burgers.CheeseBurger;
import burgers.Hamburger;
import burgers.KetchupBurger;

public class Restaurant {
	private Burger createBurger(Order order) {
		Burger burger = new Hamburger();
		if(order.isCheese()) {
			burger = new CheeseBurger(burger);
		}
		if(order.isKetchup()) {
			burger = new KetchupBurger(burger);
		}
		return burger;
	}
	
	public void createHamburgerAndThrowItAway(Order order) {
		Burger burger = createBurger(order);
		System.out.println(burger + " created but was not used :/");
	}

	public Burger createHamburger(Order order) {
		return createBurger(order);
	}
}
