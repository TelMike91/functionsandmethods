package restaurant;

public class Order {	
	private final boolean isCheese;
	private final boolean isKetchup;
	
	private Order(Builder builder) {
		isCheese = builder.isCheese;
		isKetchup = builder.isKetchup;
	}
	
	public boolean isCheese() {
		return isCheese;
	}
	
	public boolean isKetchup() {
		return isKetchup;
	}
		
	public static class Builder {
		private boolean isCheese;
		private boolean isKetchup;
		
		public Builder setIsCheese(boolean isCheese) {
			this.isCheese = isCheese;
			
			return this;
		}
		
		public Builder setIsKetchup(boolean isKetchup) {
			this.isKetchup = isKetchup;
			
			return this;
		}
		
		public Order build() {
			return new Order(this);
		}
				
	}
}
