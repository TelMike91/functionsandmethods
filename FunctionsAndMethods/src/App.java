import restaurant.Order;
import restaurant.Restaurant;

public class App {
	public static void main(String[] args) {
		Restaurant restaurant = new Restaurant();
		Customer customer = new Customer();
		
		Order ketchupBurger = new Order.Builder()
				.setIsCheese(false)
				.setIsKetchup(true)
				.build();
		
		Order cheeseAndKetchupOrder = new Order.Builder()
				.setIsCheese(true)
				.setIsKetchup(true)
				.build();
		
		restaurant.createHamburgerAndThrowItAway(ketchupBurger);
		restaurant.createHamburgerAndThrowItAway(cheeseAndKetchupOrder);
		
		customer.eatBurger(restaurant.createHamburger(ketchupBurger));
		customer.eatBurger(restaurant.createHamburger(cheeseAndKetchupOrder));
	}
}
